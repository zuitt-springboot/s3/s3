package com.zuitt.wdc044.models;

import javax.persistence.*;
@Entity
@Table(name="users")
public class User {
    @Id //indicate that this property represents the primary key
    @GeneratedValue //auto increment
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    //default constructor
    public User(){}

    //parameterized constructor
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
