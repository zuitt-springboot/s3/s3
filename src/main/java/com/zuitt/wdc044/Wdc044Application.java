package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//SpringBootApplication- annotation (import packages)
//-annotation - shortcuts that make long, repetitive code shorter
@RestController
//tell Spring Boot that this will use endpoints for handling web requests and responses
public class Wdc044Application {


	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s", name);

		//to run the app -
		//Syntax: ./mvnw spring-boot:run
	}

	//Define a hi()method that will output "Hi User" when a get request is received at localhost:8080/hi
	//A url parameter named user MAY be used which will behave similarly to our code-along (i.e. ?=user=Jane will output "Hi Jane")

	//Use simple Java concatenation instead of the String.format method

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "User") String name) {
		return "Hello " + name;
	}
}
